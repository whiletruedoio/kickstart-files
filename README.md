![Pipeline](https://gitlab.com/whiletruedoio/kickstart-files/badges/main/pipeline.svg)
![Issues](https://img.shields.io/gitlab/issues/all/whiletruedoio%2Fkickstart-files)
![Release](https://gitlab.com/whiletruedoio/kickstart-files/-/badges/release.svg)
![License](https://img.shields.io/gitlab/license/whiletruedoio%2Fkickstart-files)

# Kickstart

A collection of
[Kickstart](https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html)
files for provisioning.

## Disclaimer

I am currently migrating a lot of stuff over to GitLab. Therefore, this
repository should be considered broken or at least, under heavy development.

There are some ToDo items, that need to be tackled. You can find them in the
[TODO.md](./docs/TODO.md).

## Motivation

At [while-true-do.io](https://while-true-do.io), I wanted to automate and
document as much of the automation as possible. The initial setup of a machine
should be simple, reproducible and provide features based on the purpose of a
system. Therefore, I am aiming to provide ready-to-use configurations for the
use cases, I am considering for my setups.

## Description

The collection provides different Kickstart files for various use cases, but
also some snippets that might be handy for customization and building on top.

### Base Operating System

All of the provided snippets and templates default to **AlmaLinux OS** network
installations.

### Defaults

Be aware, all default passwords are set to "password". You **must** change these
before using the templates.

### Kickstart templates

The naming of the Kickstart files follows the following schema:

```txt
<SCOPE>_<PURPOSE>_[<ENV>]_[<SECURITY>].ks.cfg
```

Whereas:

**SCOPE** defines the general **idea** of the resulting installation.

- minimal: a minimal installation for general purpose and customization
- cockpit: a variation with the cockpit web interface
- graphical: a variation with a desktop frontend

**PURPOSE** defines some use case variants that show **how** the resulting
installation can be used.

- base: provides only the base
- podman: providing the Podman container engine
- kvm: providing the KVM hypervisor

**ENV** is optional and defines the environment **where** the image should be
used.

- phy: a physical or bare metal installation
- vir: a virtual machine on a hypervisor, including required agent packages
- cld: a machine that is intended to be running on one of the many public cloud
  providers like Amazon AWS, Azure, Exoscale, etc.

**SECURITY** is optional and defines an additional security scope, according to
[OpenSCAP](https://www.open-scap.org/).

- cis: [CIS Benchmark for Level 2 - Server](https://static.open-scap.org/ssg-guides/ssg-rhel9-guide-cis.html)
- ospp: [Protection profile for General Purpose Operating Systems](https://static.open-scap.org/ssg-guides/ssg-rhel9-guide-ospp.html)

In addition, you might find special named Kickstart files. All of them having
some additional documentation in the Usage documentation or the header of the
Kickstart file.

### Kickstart Snippets

Snippets are meant to provide some useful copy-paste-templates for
customization. Since it is basically impossible to have templates for everything
these snippets shall help to support with some rare or edge cases.

## Usage

The usage of this repository and its code is explained in the
[Usage documentation](./docs/USAGE.md).

## Contribute

Thank you so much for considering to contribute. Please don't hesitate to
provide feedback, report bugs, request features or contribute code. Please
make yourself comfortable with the
[contributing guidelines](https://gitlab.com/whiletruedoio/gitlab-profile/-/blob/main/docs/CONTRIBUTING.md)
first.

### Issues

Opening issues and reporting bugs is pretty easy. Please feel free to open a
report via the issue tracker or send an e-mail.

- [Issue Tracker](https://gitlab.com/whiletruedoio/kickstart-files/-/issues)
- [Support Mail](mailto:support@while-true-do.io)

### Development

In case you consider to contribute with your development, please check out the
[development documentation](./docs/DEVELOPMENT.md).

## License

Except otherwise noted, all work is [licensed](LICENSE) under a
[BSD-3-Clause License](https://opensource.org/licenses/BSD-3-Clause).

## Contact

In case you want to get in touch with me or reach out for general questions,
please use one of the below contact details

- Blog: [blog.while-true-do.io](https://while-true-do.io)
- Code: [gitlab.com/whiletruedoio](https://gitlab.com/whiletruedoio)
- Chat: [#whiletruedoio-community](https://matrix.to/#/#whiletruedoio-community:matrix.org)
- Mail: [hello@while-true-do.io](mailto:hello@while-true-do.io)
