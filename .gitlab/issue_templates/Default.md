<!--
Requesting issues should follow some simple rules:

- **Check**, if the issue was already reported
- **Describe** what your issue report will bring to the community
- **Explain** the criteria to fulfill the request
- **Add** more details like mock-ups, attachments, lists, screenshots
- **Follow Up** in the discussion to the feature
-->

## Description

<!-- Please describe the issue and why it will be useful to the community. -->

## Criteria

<!-- Please explain the criteria, so the issue is solved for you. -->

## Additional information

<!-- Please add information like mockups, code snippets, flow diagrams, etc. -->
