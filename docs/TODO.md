# ToDo

This small list includes some ToDo items, I might tackle soon'ish.

## Mandatory additions

- [ ] Testing
  - [ ] ksvalidator
  - [ ] some kind of integration testing
  - [ ] CI/CD for testing
- [ ] documentation
  - [ ] Usage
  - [ ] Develop
  - [ ] CI/CD to generate mkdocs

## Mandatory templates

- [ ] minimal_base
- [ ] minimal_base_ospp
- [x] minimal_base_cisl1
- [ ] minimal_base_cisl2
- [ ] minimal_podman
- [ ] minimal_kvm
- [ ] cockpit_base
- [ ] cockpit_podman
- [ ] cockpit_kvm

## Optional templates

- [ ] minimal_iot

## Mandatory Snippets

- [ ] ansible pull
- [ ] interactive prompts
